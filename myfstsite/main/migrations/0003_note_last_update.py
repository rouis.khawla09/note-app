# Generated by Django 4.0.1 on 2022-01-30 13:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_alter_note_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='last_update',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
